INSTALLATION
============

RPMs
~~~~

Binary, debuginfo and source RPMs for VMOD re2 are available at
packagecloud:

	https://packagecloud.io/uplex/varnish

The packages are built for Enterprise Linux 7 (el7), and hence will
run on compatible distros (such as RHEL7, Fedora, CentOS 7 and Amazon
Linux).

To set up your YUM repository for the RPMs, follow these instructions:

	https://packagecloud.io/uplex/varnish/install#manual-rpm

You will also need these additional repositories:

* EPEL7

  * ``yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm``

  * EPEL provides packages for the RE2 library, at least version
    2015-05-01 is required.

* Official Varnish packages from packagecloud (since version 5.2.0)

  * Follow the instructions at: https://packagecloud.io/varnishcache/varnish52/install#manual-rpm

If you have problems or questions concerning the RPMs, post an issue
to one of the source repository web sites, or contact
<varnish-support@uplex.de>.

Building from source
~~~~~~~~~~~~~~~~~~~~

The VMOD is built against a Varnish installation, and the autotools
use ``pkg-config(1)`` to locate the necessary header files and other
resources for both Varnish and RE2. This sequence will install the VMOD::

  > ./autogen.sh	# for builds from the git repo
  > ./configure
  > make
  > make check		# to run unit tests in src/tests/*.vtc
  > make distcheck	# run check and prepare a distribution tarball
  > sudo make install

The ``autogen.sh`` and ``configure`` steps require m4 sources from the
Varnish installation and from the autoconf archive. If you encounter
errors in those steps (such as ``Need varnish.m4 -- see
INSTALL.rst``):

* Make sure that you have a Varnish installation on the same host, and
  that its ``$PREFIX/share/aclocal`` directory can be found by
  ``aclocal(1)``. If necessary, set the ``ACLOCAL_PATH`` environment
  variable to include that path.

* If you have installed Varnish and/or RE2 in non-standard
  directories, call ``autogen.sh`` and ``configure`` with the
  ``PKG_CONFIG_PATH`` environment variable set to include the paths
  where the ``.pc`` files can be located for ``varnishapi`` and
  ``re2``. For example, when varnishd configure was called with
  ``--prefix=$PREFIX``, use::

  > PKG_CONFIG_PATH=${PREFIX}/lib/pkgconfig
  > export PKG_CONFIG_PATH

* Make sure that you have the autoconf archive installed (available as
  the package ``autoconf-archive`` on most distributions).

The VMOD source code is in C and C++, since the RE2 API is C++. So you
will need both C and C++ compilers, such as gcc/g++ or clang/clang++.

See `CONTRIBUTING.rst <CONTRIBUTING.rst>`_ for more details about
building from source.

By default, the vmod ``configure`` script installs the vmod in
the same directory as Varnish, determined via ``pkg-config(1)``. The
vmod installation directory can be overridden by passing the
``VMOD_DIR`` variable to ``configure``.

Other files such as the man-page are installed in the locations
determined by ``configure``, which inherits its default ``--prefix``
setting from Varnish.
