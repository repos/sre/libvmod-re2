#! /bin/bash -ex

# Build script for a VMOD re2 el7 RPM
# to be run in the rpmbuild/centos7 docker container.

# Env variables VERSION and RELEASE MUST be set in the docker invocation.
# DIST is set by the rpmbuild/centos7 container.

# The directory mounted to /srv MUST contain these files:
# - this script (named pkg)
# - VMOD re2 tarball (vmod-re2-$VERSION.tar.gz)
# - RPM spec file (vmod-re2.spec)
# - yum repo config for varnish60@packagecloud (varnishcache_varnish60.repo)
#   (see https://packagecloud.io/varnishcache/varnish60/install#manual-rpm)

# So this is a sample docker invocation:
#
# $ docker run -t -v $PWD:/srv -e VERSION=1.2.3 -e RELEASE=4711 \
#	rpmbuild/centos7 /srv/pkg

# At the end of the run, binary, source and debuginfo RPMs are in the
# directory mounted to /srv.

if [ -z $VERSION ]; then
   echo "Env variable VERSION MUST be set"
   exit 1
fi

if [ -z $RELEASE ]; then
   echo "Env variable RELEASE MUST be set"
   exit 1
fi

# delete the peculiar macros from the rpmbuild/centos7 image
rm /home/builder/.rpmmacros

# set up the build environment
cd /home/builder
mkdir -p rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}
cp /srv/vmod-re2.spec rpmbuild/SPECS
cp /srv/vmod-re2-${VERSION}.tar.gz rpmbuild/SOURCES

# install epel7 repo
sudo yum install -y -q \
     https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

# set up varnish repo
sudo cp /srv/varnishcache_varnish60.repo /etc/yum.repos.d/

sudo yum -q makecache -y --disablerepo='*' --enablerepo='varnishcache_varnish60'

# build requirements
sudo yum install -y -q \
     varnish-devel re2-devel pkgconfig make gcc gcc-c++ python-docutils

# build RPMs
rpmbuild -ba -D "dist .${DIST}" \
         -D "_version ${VERSION}" \
         -D "_release ${RELEASE}" \
         rpmbuild/SPECS/vmod-re2.spec

sudo cp rpmbuild/RPMS/*/* /srv
sudo cp rpmbuild/SRPMS/* /srv
