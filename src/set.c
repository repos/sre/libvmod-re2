/*-
 * Copyright (c) 2017 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "vmod_re2.h"

#include <stdio.h>

#include "vbm.h"
#include "vre2/vre2set.h"

#define INIT(ctx) (((ctx)->method & VCL_MET_INIT) != 0)

enum bitmap_e {
	STRING = 0,
	BACKEND,
	REGEX,
	INTEGER,
	__MAX_BITMAP,
};

#define NELEMS(a) (sizeof(a) / sizeof((a)[0]))

struct set_options {
	VCL_INT		max_mem;
	unsigned	utf8 : 1;
	unsigned	posix_syntax : 1;
	unsigned	longest_match : 1;
	unsigned	literal : 1;
	unsigned	never_nl : 1;
	unsigned	dot_nl : 1;
	unsigned	case_sensitive : 1;
	unsigned	perl_classes : 1;
	unsigned	word_boundary : 1;
	unsigned	one_line : 1;
};

struct vmod_re2_set {
	unsigned		magic;
#define VMOD_RE2_SET_MAGIC 0xf6d7b15a
	vre2set			*set;
	struct vbitmap		*added[__MAX_BITMAP];
	char			*vcl_name;
	char			**string;
	VCL_BACKEND		*backend;
	struct vmod_re2_regex	**regex;
	VCL_INT			*integer;
	struct set_options	opts;
	unsigned		compiled;
	int			npatterns;
};

struct task_set_match {
	unsigned	magic;
#define TASK_SET_MATCH_MAGIC 0x7a24a90b
	int		*matches;
	size_t		nmatches;
};

typedef
VCL_STRING regex_rewrite_f(const struct vrt_ctx *ctx, struct vmod_re2_regex *re,
			   VCL_STRING text, VCL_STRING rewrite,
			   VCL_STRING fallback);

static regex_rewrite_f * const regex_rewrite[] = {
	[SUB]		= vmod_regex_sub,
	[SUBALL]	= vmod_regex_suball,
	[EXTRACT]	= vmod_regex_extract,
};

static inline int
decimal_digits(int n)
{
	int digits = 1;

	assert(n >= 0);
	while (n > 9) {
		digits++;
		n /= 10;
	}
	return digits;
}

/* Object set */

VCL_VOID
vmod_set__init(VRT_CTX, struct vmod_re2_set **setp, const char *vcl_name,
	       VCL_ENUM anchor, VCL_BOOL utf8, VCL_BOOL posix_syntax,
	       VCL_BOOL longest_match, VCL_INT max_mem, VCL_BOOL literal,
	       VCL_BOOL never_nl, VCL_BOOL dot_nl, VCL_BOOL case_sensitive,
	       VCL_BOOL perl_classes, VCL_BOOL word_boundary, VCL_BOOL one_line)
{
	struct vmod_re2_set *set;
	anchor_e anchor_e;
	const char *err;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(setp);
	AZ(*setp);
	AN(vcl_name);
	AN(anchor);
	ALLOC_OBJ(set, VMOD_RE2_SET_MAGIC);
	AN(set);
	*setp = set;

	if (strcmp(anchor, "none") == 0)
		anchor_e = NONE;
	else if (strcmp(anchor, "start") == 0)
		anchor_e = START;
	else if (strcmp(anchor, "both") == 0)
		anchor_e = BOTH;
	else
		WRONG("illegal anchor");

	if ((err = vre2set_init(&set->set, anchor_e, utf8, posix_syntax,
				longest_match, max_mem, literal, never_nl,
				dot_nl, case_sensitive, perl_classes,
				word_boundary, one_line))
	    != NULL) {
		VERR(ctx, "new %s = re2.set(): %s", vcl_name, err);
		return;
	}

	for (unsigned i = 0; i < NELEMS(set->added); i++) {
		set->added[i] = vbit_new(0);
		AN(set->added[i]);
	}

	set->vcl_name = strdup(vcl_name);
	AN(set->vcl_name);

#define SET(OPT) set->opts.OPT = OPT;
	SET(utf8);
	SET(posix_syntax);
	SET(longest_match);
	SET(max_mem);
	SET(literal);
	SET(never_nl);
	SET(dot_nl);
	SET(case_sensitive);
	SET(perl_classes);
	SET(word_boundary);
	SET(one_line);
#undef SET

	AZ(set->string);
	AZ(set->backend);
	AZ(set->regex);
	AZ(set->compiled);
	AZ(set->npatterns);
}

VCL_VOID
vmod_set__fini(struct vmod_re2_set **setp)
{
	struct vmod_re2_set *set;

	if (setp == NULL || *setp == NULL)
		return;
	CHECK_OBJ(*setp, VMOD_RE2_SET_MAGIC);
	set = *setp;
	*setp = NULL;
	vre2set_fini(&set->set);
	for (int i = 0; i < set->npatterns; i++) {
		if (vbit_test(set->added[STRING], i)
		    && set->string[i] != NULL)
			free(set->string[i]);
		if (vbit_test(set->added[REGEX], i)
		    && set->regex[i] != NULL)
			vmod_regex__fini(&set->regex[i]);
	}
	for (unsigned i = 0; i < NELEMS(set->added); i++)
		vbit_destroy(set->added[i]);
	if (set->vcl_name != NULL)
		free(set->vcl_name);
	FREE_OBJ(set);
}

#define ERR_PREFIX "%s.add(\"%.40s\"): "

VCL_VOID
vmod_set_add(VRT_CTX, struct vmod_re2_set *set, struct vmod_set_add_arg *args)
{
	const char *err;
	int n;
	VCL_STRING pattern = args->arg1;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(set, VMOD_RE2_SET_MAGIC);
	if (pattern == NULL)
		pattern = "";
	if (!INIT(ctx)) {
		VERR(ctx, ERR_PREFIX ".add() may only be called in vcl_init",
		     set->vcl_name, pattern);
		return;
	}

	if (set->compiled) {
		VERR(ctx, ERR_PREFIX "%s has already been compiled",
		     set->vcl_name, pattern, set->vcl_name);
		return;
	}
	if ((err = vre2set_add(set->set, pattern, &n)) != NULL) {
		VERR(ctx, ERR_PREFIX "Cannot compile '%.40s': %s",
		     set->vcl_name, pattern, pattern, err);
		return;
	}

	if (args->valid_string && args->string != NULL) {
		if ((set->string = realloc(set->string,
					   (n + 1) * (sizeof(char *))))
		    == NULL) {
			VERRNOMEM(ctx, ERR_PREFIX "adding string %s",
				   set->vcl_name, pattern, args->string);
			return;
		}
		set->string[n] = strdup(args->string);
		AN(set->string[n]);
		vbit_set(set->added[STRING], n);
	}
	if (args->valid_backend && args->backend != NULL) {
		if ((set->backend = realloc(set->backend,
					    (n + 1) * (sizeof(VCL_BACKEND))))
		    == NULL) {
			VERRNOMEM(ctx, ERR_PREFIX "adding backend %s",
				   set->vcl_name, pattern,
				   VRT_BACKEND_string(args->backend));
			return;
		}
		set->backend[n] = args->backend;
		vbit_set(set->added[BACKEND], n);
	}
	if (args->valid_integer) {
		if ((set->integer = realloc(set->integer,
					    (n + 1) * (sizeof(VCL_INT))))
		    == NULL) {
			VERRNOMEM(ctx, ERR_PREFIX "adding integer %jd",
				   set->vcl_name, pattern, args->integer);
			return;
		}
		set->integer[n] = args->integer;
		vbit_set(set->added[INTEGER], n);
	}
	if (args->valid_save && args->save) {
		struct vmod_re2_regex *re = NULL;
		char *vcl_name;
		size_t namelen;
		int digits = decimal_digits(n);

		assert(digits >= 1);
		namelen = strlen(set->vcl_name) + digits + 2;
		vcl_name = malloc(namelen);
		snprintf(vcl_name, namelen, "%s_%d", set->vcl_name, n);
		if (!args->valid_never_capture)
			args->never_capture = 0;
		vmod_regex__init(ctx, &re, vcl_name, pattern, set->opts.utf8,
				 set->opts.posix_syntax,
				 set->opts.longest_match, set->opts.max_mem,
				 set->opts.literal, set->opts.never_nl,
				 set->opts.dot_nl, args->never_capture,
				 set->opts.case_sensitive,
				 set->opts.perl_classes,
				 set->opts.word_boundary, set->opts.one_line);
		free(vcl_name);
		if (re->vcl_name == NULL) {
			vmod_regex__fini(&re);
			return;
		}
		if ((set->regex
		     = realloc(set->regex,
			       (n + 1) * (sizeof(struct vmod_re2_regex *))))
		    == NULL) {
			VERRNOMEM(ctx, ERR_PREFIX "saving regex", set->vcl_name,
				  pattern);
			return;
		}
		set->regex[n] = re;
		vbit_set(set->added[REGEX], n);
	}
	set->npatterns++;
}

#undef ERR_PREFIX

#define ERR_PREFIX "%s.compile(): "

VCL_VOID
vmod_set_compile(VRT_CTX, struct vmod_re2_set *set)
{
	const char *err;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(set, VMOD_RE2_SET_MAGIC);
	if (!INIT(ctx)) {
		VERR(ctx, ERR_PREFIX ".compile() may only be called in "
		     "vcl_init", set->vcl_name);
		return;
	}
	if (set->npatterns == 0) {
		VERR(ctx, ERR_PREFIX "no patterns were added", set->vcl_name);
		return;
	}

	if (set->compiled) {
		VERR(ctx, ERR_PREFIX "%s has already been compiled",
		     set->vcl_name, set->vcl_name);
		return;
	}
	if ((err = vre2set_compile(set->set)) != NULL) {
		VERR(ctx, ERR_PREFIX "failed, possibly insufficient memory",
		     set->vcl_name);
		return;
	}
	set->compiled = 1;
}

#undef ERR_PREFIX

#define ERR_PREFIX "%s.match(\"%.40s\"): "

VCL_BOOL
vmod_set_match(VRT_CTX, struct vmod_re2_set *set, VCL_STRING subject)
{
	int match = 0;
	struct vmod_priv *priv;
	struct task_set_match *task;
	char *buf;
	size_t buflen;
	const char *err;
	errorkind_e errkind = NO_ERROR;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(set, VMOD_RE2_SET_MAGIC);
	if (subject == NULL)
		subject = "";

	if (!set->compiled) {
		VERR(ctx, ERR_PREFIX "%s was not compiled", set->vcl_name,
		     subject, set->vcl_name);
		return 0;
	}

	priv = VRT_priv_task(ctx, set);
	AN(priv);
	if (priv->priv == NULL) {
		if ((priv->priv = WS_Alloc(ctx->ws, sizeof(*task))) == NULL) {
			VERRNOMEM(ctx, ERR_PREFIX "allocating match data",
				  set->vcl_name, subject);
			return 0;
		}
		priv->len = sizeof(*task);
		priv->free = NULL;
		task = priv->priv;
		task->magic = TASK_SET_MATCH_MAGIC;
	}
	else {
		WS_Assert_Allocated(ctx->ws, priv->priv, sizeof(*task));
		CAST_OBJ(task, priv->priv, TASK_SET_MATCH_MAGIC);
	}

	buf = WS_Front(ctx->ws);
	buflen = WS_Reserve(ctx->ws, 0);
	if ((err = vre2set_match(set->set, subject, &match, buf, buflen,
				 &task->nmatches, &errkind)) != NULL) {
		VERR(ctx, ERR_PREFIX "%s", set->vcl_name, subject, err);
		WS_Release(ctx->ws, 0);
		return 0;
	}
	if (match) {
		task->matches = (int *)buf;
		WS_Release(ctx->ws, task->nmatches * sizeof(int));
	}
	else {
		WS_Release(ctx->ws, 0);

		switch(errkind) {
		case NO_ERROR:
		case NOT_IMPLEMENTED:
			break;
		case OUT_OF_MEMORY:
			VERR(ctx, ERR_PREFIX "RE2 lib indicates out-of-memory "
			     "during match, consider increasing max_mem",
			     set->vcl_name, subject);
			break;
		case NOT_COMPILED:
		case INCONSISTENT:
		default:
			WRONG("impossible or invalid error kind");
		}
	}

	return match;
}

#undef ERR_PREFIX

static struct task_set_match *
get_task_data(VRT_CTX, struct vmod_re2_set *set)
{
	struct vmod_priv *priv;
	struct task_set_match *task;

	priv = VRT_priv_task(ctx, set);
	AN(priv);
	if (priv->priv == NULL)
		return NULL;
	WS_Assert_Allocated(ctx->ws, priv->priv, sizeof(*task));
	CAST_OBJ(task, priv->priv, TASK_SET_MATCH_MAGIC);
	return task;
}

VCL_BOOL
vmod_set_matched(VRT_CTX, struct vmod_re2_set *set, VCL_INT n)
{
	struct task_set_match *task;
	int hi, lo = 0;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(set, VMOD_RE2_SET_MAGIC);
	if (n < 1 || n > set->npatterns) {
		VERR(ctx, "n=%d out of range in %s.matched() (%d patterns)", n,
		     set->vcl_name, set->npatterns);
		return 0;
	}

	if ((task = get_task_data(ctx, set)) == NULL) {
		VERR(ctx, "%s.matched(%d) called without prior match",
		     set->vcl_name, n);
		return 0;
	}

	if (task->nmatches == 0)
		return 0;
	WS_Assert_Allocated(ctx->ws, task->matches,
			    task->nmatches * sizeof(int));
	n--;
	hi = task->nmatches;
	do {
		int m = lo + (hi - lo) / 2;
		if (task->matches[m] == n)
			return 1;
		if (task->matches[m] < n)
			lo = m + 1;
		else
			hi = m - 1;
	} while (lo <= hi);

	return 0;
}

VCL_INT
vmod_set_nmatches(VRT_CTX, struct vmod_re2_set *set)
{
	struct task_set_match *task;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(set, VMOD_RE2_SET_MAGIC);
	if ((task = get_task_data(ctx, set)) == NULL) {
		VERR(ctx, "%s.nmatches() called without prior match",
		     set->vcl_name);
		return 0;
	}
	return task->nmatches;
}

static int
get_match_idx(VRT_CTX, struct vmod_re2_set *set, VCL_INT n, VCL_ENUM selects,
	      const char *method)
{
	struct task_set_match *task;
	int idx = 0;

	if (n > set->npatterns) {
		VERR(ctx, "%s.%s(%lld): set has %d patterns", set->vcl_name,
		     method, n, set->npatterns);
		return -1;
	}
	if (n > 0)
		return n - 1;

	if ((task = get_task_data(ctx, set)) == NULL) {
		VERR(ctx, "%s.%s() called without prior match", set->vcl_name,
		     method);
		return -1;
	}

	if (task->nmatches == 0) {
		VERR(ctx, "%s.%s(%lld): previous match was unsuccessful",
		     set->vcl_name, method, n);
		return -1;
	}
	if (task->nmatches > 1) {
		if (selects == vmod_enum_UNIQUE) {
			VERR(ctx, "%s.%s(%lld): %d successful matches",
			     set->vcl_name, method, n, task->nmatches);
			return -1;
		}
		if (selects == vmod_enum_LAST)
			idx = task->nmatches - 1;
		else
			assert(selects == vmod_enum_FIRST);
	}
	WS_Assert_Allocated(ctx->ws, task->matches,
			    task->nmatches * sizeof(int));
	return task->matches[idx];
}

VCL_INT
vmod_set_which(VRT_CTX, struct vmod_re2_set *set, VCL_ENUM selects)
{
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(set, VMOD_RE2_SET_MAGIC);
	return get_match_idx(ctx, set, 0, selects, "which") + 1;
}

static VCL_STRING
rewritef(VRT_CTX, struct vmod_re2_set * const restrict set,
	 VCL_STRING const restrict text, VCL_STRING const restrict rewrite,
	 VCL_STRING const restrict fallback, VCL_INT n,
	 VCL_ENUM const restrict selects, rewrite_e type)
{
	int idx;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(set, VMOD_RE2_SET_MAGIC);
	if (set->regex == NULL) {
		VERR(ctx, "%s.%s(%lld): No regexen were saved for %s",
		     set->vcl_name, rewrite_name[type], n, set->vcl_name);
		return NULL;
	}

	idx = get_match_idx(ctx, set, n, selects, rewrite_name[type]);
	if (idx < 0)
		return NULL;
	if (!vbit_test(set->added[REGEX], idx)) {
		AN(selects);
		VERR(ctx, "%s.%s(%s, %s, %lld, %s): Pattern %d was not saved",
		     set->vcl_name, rewrite_name[type], text, rewrite, n,
		     selects, idx + 1);
		return NULL;
	}
	return (regex_rewrite[type])(ctx, set->regex[idx], text, rewrite,
				     fallback);
}

VCL_STRING
vmod_set_sub(VRT_CTX, struct vmod_re2_set *set, VCL_STRING text,
	     VCL_STRING rewrite, VCL_STRING fallback, VCL_INT n,
	     VCL_ENUM selects)
{
	return rewritef(ctx, set, text, rewrite, fallback, n, selects, SUB);
}

VCL_STRING
vmod_set_suball(VRT_CTX, struct vmod_re2_set *set, VCL_STRING text,
		VCL_STRING rewrite, VCL_STRING fallback, VCL_INT n,
		VCL_ENUM selects)
{
	return rewritef(ctx, set, text, rewrite, fallback, n, selects, SUBALL);
}

VCL_STRING
vmod_set_extract(VRT_CTX, struct vmod_re2_set *set, VCL_STRING text,
		 VCL_STRING rewrite, VCL_STRING fallback, VCL_INT n,
		 VCL_ENUM selects)
{
	return rewritef(ctx, set, text, rewrite, fallback, n, selects, EXTRACT);
}

VCL_STRING
vmod_set_string(VRT_CTX, struct vmod_re2_set *set, VCL_INT n, VCL_ENUM selects)
{
	int idx;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(set, VMOD_RE2_SET_MAGIC);
	if (set->string == NULL) {
		VERR(ctx, "%s.string(%lld): No strings were set for %s",
		     set->vcl_name, n, set->vcl_name);
		return NULL;
	}

	idx = get_match_idx(ctx, set, n, selects, "string");
	if (idx < 0)
		return NULL;
	if (!vbit_test(set->added[STRING], idx)) {
		AN(selects);
		VERR(ctx, "%s.string(%lld, %s): String %lld was not added",
		     set->vcl_name, n, selects, idx + 1);
		return NULL;
	}
	return set->string[idx];
}

VCL_BACKEND
vmod_set_backend(VRT_CTX, struct vmod_re2_set *set, VCL_INT n, VCL_ENUM selects)
{
	int idx;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(set, VMOD_RE2_SET_MAGIC);
	if (set->backend == NULL) {
		VERR(ctx, "%s.backend(%lld): No backends were set for %s",
		     set->vcl_name, n, set->vcl_name);
		return NULL;
	}

	idx = get_match_idx(ctx, set, n, selects, "backend");
	if (idx < 0)
		return NULL;
	if (!vbit_test(set->added[BACKEND], idx)) {
		AN(selects);
		VERR(ctx, "%s.backend(%lld, %s): Backend %lld was not added",
		     set->vcl_name, n, selects, idx + 1);
		return NULL;
	}
	return set->backend[idx];
}

VCL_INT
vmod_set_integer(VRT_CTX, struct vmod_re2_set *set, VCL_INT n, VCL_ENUM selects)
{
	int idx;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(set, VMOD_RE2_SET_MAGIC);
	if (set->integer == NULL) {
		VERR(ctx, "%s.integer(%jd): No integers were set for %s",
		     set->vcl_name, n, set->vcl_name);
		*ctx->handling = VCL_RET_FAIL;
		return (0);
	}

	idx = get_match_idx(ctx, set, n, selects, "integer");
	if (idx < 0) {
		*ctx->handling = VCL_RET_FAIL;
		return (0);
	}
	if (!vbit_test(set->added[INTEGER], idx)) {
		AN(selects);
		VERR(ctx, "%s.integer(%jd, %s): Integer %jd was not added",
		     set->vcl_name, n, selects, idx + 1);
		*ctx->handling = VCL_RET_FAIL;
		return (0);
	}
	return set->integer[idx];
}

VCL_BOOL
vmod_set_saved(VRT_CTX, struct vmod_re2_set *set, VCL_ENUM whichs, VCL_INT n,
	       VCL_ENUM selects)
{
	int idx;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(set, VMOD_RE2_SET_MAGIC);

	idx = get_match_idx(ctx, set, n, selects, "saved");
	if (idx < 0)
		return 0;
	if (whichs == vmod_enum_REGEX)
		return vbit_test(set->added[REGEX], idx);
	if (whichs == vmod_enum_BE)
		return vbit_test(set->added[BACKEND], idx);
	if (whichs == vmod_enum_STR)
		return vbit_test(set->added[STRING], idx);
	if (whichs == vmod_enum_INT)
		return vbit_test(set->added[INTEGER], idx);
	WRONG("illegal which ENUM");
	return 0;
}
