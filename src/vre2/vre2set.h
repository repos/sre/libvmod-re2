/*-
 * Copyright (c) 2016 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#ifndef _VRE2SET_H
#define _VRE2SET_H

typedef enum {
	NO_ERROR = 0,
	NOT_COMPILED,
	OUT_OF_MEMORY,
	INCONSISTENT,
	NOT_IMPLEMENTED
} errorkind_e;

#ifdef __cplusplus

#include <re2/re2.h>
#include <re2/set.h>

using namespace re2;
using std::string;

class vre2set {
private:
	RE2::Set* set_;

public:
	vre2set(RE2::Options * const opt, const RE2::Anchor anchor);
	virtual ~vre2set();
	int add(const char* pattern, string* error) const;
	bool compile() const;
	bool match(const char* subject, std::vector<int>* m,
		   errorkind_e* err) const;
};
#else
typedef struct vre2set vre2set;
#endif

typedef enum {
	NONE,
	START,
	BOTH
} anchor_e;

#ifdef __cplusplus
extern "C" {
#endif

	const char *vre2set_init(vre2set **set, anchor_e anchor, unsigned utf8,
				 unsigned posix_syntax, unsigned longest_match,
				 long max_mem, unsigned literal,
				 unsigned never_nl, unsigned dot_nl,
				 unsigned case_sensitive, unsigned perl_classes,
				 unsigned word_boundary, unsigned one_line);
	const char *vre2set_fini(vre2set **vre2);
	const char *vre2set_add(vre2set *set, const char *pattern,
				int * const idx);
	const char *vre2set_compile(vre2set *set);
	const char *vre2set_match(vre2set *set, const char *subject,
				  int * const match, void *buf,
				  const size_t buflen, size_t * const nmatches,
				  errorkind_e * const err);

#ifdef __cplusplus
}
#endif

#endif /* _VRE2SET_H */
